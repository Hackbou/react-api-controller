import Case from "case";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (plop) {
  /*
    |--------------------------------------------------------------------------
    | GENERATOR FOR REDUX RESOURCES: APICONTROLLER & APIRESOURCE
    |--------------------------------------------------------------------------
    */
  plop.setGenerator("plop", {
    description: "Code faster",
    prompts: [
      {
        type: "input",
        name: "name"
      },
    ],
    actions: function (answers) {
      const name = answers.name;
      return [
        {
          type: "add",
          path: `../../src/redux/apiController/${name}Api.js`,
          templateFile: "./lib/reduxResource/apiControler.template.hbs",
          skipIfExists: true,
        },
        {
          type: "add",
          path: `../../src/redux/features/${name}Slice.js`,
          templateFile: "./lib/reduxResource/reducer.template.hbs",
          skipIfExists: true,
        },
      ];
    },
  });


  /*
    |--------------------------------------------------------------------------
    | GENERATOR FOR REDUX STORE BASIC CONFIG
    |--------------------------------------------------------------------------
    */
  plop.setGenerator("store", {
    description: "Code faster",
    prompts: [
      {
        type: "input",
        name: "name"
      },
    ],
    actions: function (answers) {
      const name = answers.name;
      return [
        {
          type: "add",
          path: `../../src/redux/${name}.js`,
          templateFile: "./lib/reduxStore/store.template.hbs",
          skipIfExists: true,
        },
      ];
    },
  });

  plop.setHelper("pascalCase", (str) => Case.pascal(str));
  plop.setHelper("camelCase", (str) => Case.camel(str));
}

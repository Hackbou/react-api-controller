import fs from "fs";
import path from "path";
import chalk from "chalk";
import { program } from "commander";
import { log } from "console";

import fsExtra from "fs-extra";

// Chemin du fichier de configuration
const fichierConfig = path.join(process.cwd(), "redteam.config.json");

// Définir la version et la description de votre programme
program.version("1.0.0").description("Générateur de fichiers");

// Définir la commande "generate"
program
  .command("make <commande> <nom_fichier>")
  .description("Génère un fichier à partir d'une commande spécifiée")
  .action((commande, nomResource) => {


    /**
     * Pour eviter d'avoir des saisies comme: exemple.ts, exemple.js, etc
     */
    let nomFichier = nomResource.split('.')[0]

    fs.readFile(fichierConfig, "utf8", async (err, data) => {
      // log({ commande });
      if (err) {
        log(
          chalk.red.bold(
            "Une erreur s'est produite lors de la lecture du fichier de configuration :"
          ),
          err
        );
        return;
      }

      //! recuperer le contenu JSON du fichier de configuration
      let dossierConfig;
      try {
        dossierConfig = JSON.parse(data);
      } catch (err) {
        console.error(
          "Une erreur s'est produite lors de l'analyse du fichier de configuration JSON :",
          err
        );
        return;
      }

      // Vérifier si la commande existe dans la configuration
      if (!dossierConfig.hasOwnProperty(commande)) {
        console.error(
          "La commande spécifiée n'existe pas dans le fichier de configuration."
        );
        return;
      }

      //! dossier ou fichier a copier depuis le fichier de configuration
      const dossierOuFichierACopier = dossierConfig[commande];

      // Vérifier si le dossier de destination existe
      if (!fs.existsSync(dossierOuFichierACopier)) {
        console.error(
          "Le dossier de destination n'existe pas :",
          dossierOuFichierACopier
        );
        return;
      }



      /*
    |--------------------------------------------------------------------------
    | GENERATE REDUX RESOURCE
    |--------------------------------------------------------------------------
    */
      if (commande === "reduxResource") {
        console.log(
          chalk.blue.bgGreen.bold(
            `******************* ${nomFichier} api controller *****************`
          )
        );
        (async () => {
          const nodePlop = (await import("node-plop")).default;

          //! recupere l'instance plop dans le fichier plopfile
          const plop = await nodePlop(`./plopfile.js`);
          //! getter un generator
          const basicAdd = plop.getGenerator("plop");

          // run all the generator actions using the data specified
          basicAdd
            .runActions({ name: nomFichier })
            .then(function ({ changes, failures }) {
              // do something after the actions have run
              changes.forEach((change) => {
                console.info(`${change.path} created successfully.`);
              });
            });
        })();
        return;
      }


      /*
    |--------------------------------------------------------------------------
    | GENERATE REDUX STORE
    |--------------------------------------------------------------------------
    */
      if (commande === 'redux') {
        console.log(
          chalk.blue.cyanBright.bold(
            `**************** redux store ***************`
          )
        );
        (async () => {
          const nodePlop = (await import("node-plop")).default;

          //! recupere l'instance plop dans le fichier plopfile
          const plop = await nodePlop(`./plopfile.js`);
          //! getter un generator
          const basicAdd = plop.getGenerator("store");

          // run all the generator actions using the data specified
          basicAdd
            .runActions({ name: nomFichier })
            .then(function ({ changes, failures }) {
              // do something after the actions have run
              changes.forEach((change) => {
                console.info(`${change.path} created successfully.`);
              });
            });
          // log({ nodePlop })
        })();
        return
      }


      /**=================================================
       * GENERATE AUTHENTICATION PAGES
       * ==================================================
       */
      if (commande === "auth") {
        const dossierDestination = path.join("src/", nomFichier);
        copyFolder(dossierOuFichierACopier, dossierDestination);
        return;
      }

      // Chemin complet du fichier de destination
      const fichierDestination = path.join("src/", nomFichier);

      log({ fichierDestination });
      // Vérifier si le fichier de destination existe déjà
      if (fs.existsSync(fichierDestination)) {
        console.error(
          "Le fcicher : " +
            chalk.red.bgRed(chalk.green.bold(nomFichier) + " existe déjà")
        );
        return;
      }

      copyFile(dossierOuFichierACopier, fichierDestination);
    });
    return;
  });

/**
 *
 * @param {string} file
 * @param {string} destination
 * @return {Boolean}
 */
function copyFile(file, destination) {
  fs.copyFile(file, destination, (err) => {
    if (err) {
      return false;
    }
  });
  return true;
}

/**
 *
 * @param {string} folder
 * @param {string} destination
 * @returns {Boolean}
 */
function copyFolder(folder, destination) {
  if (fs.existsSync(destination)) {
    console.error("Ce dossier existe deja: ", destination);
    return;
  }
  fsExtra.copy(folder, destination, (error) => {
    if (error) {
      log({ FOLDERCOPYERROR: error });
      return false;
    }
    return true;
  });
}

// Analyser les arguments de la ligne de commande
export function generateFile(args) {
  program.parse(args);
}

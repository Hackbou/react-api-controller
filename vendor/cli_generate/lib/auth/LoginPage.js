import Button from "components/Button";
import Input from "components/Input";
import withHandleForm from "hoc/withHandleForm";
import React from "react";
import { FaPassport, FaPhone } from "react-icons/fa";
import { Link } from "react-router-dom";
import { REGISTER_PATH } from "routes/navigation/navigationPaths";
/**
 * 
 * @param{}
 * @returns
 * 
 */

const LoginPage = withHandleForm(({ form, handleChange }) => {

  const handleLogin = async (e) => {
    e.preventDefault()
    console.log(form);
  };

  // TODO: ameliorer le style: inline style
  // TODO: creer de mini library
  // ! justifier le choix des packages

  return (
    <div style={{ width: "100%", marginTop: '2rem' }}>
      <form style={{ maxWidth: '500px', margin: 'auto', padding: '0.5rem' }}>
        <h5 style={{ textAlign: 'center', fontSize: '1.1rem' }}>
          Bonjour , Bienvenue sur Red team boilerplate
        </h5>
        <Input
          type={"tel"}
          label={"77 XXX XX XX"}
          icon={{ name: FaPhone }}
          style={{ marginBottom: 20 }}
          name="phone"
          value={form.phone}
          onChange={handleChange}
        />
        <Input
          type={"password"}
          label={"Mot de passe"}
          icon={{ name: FaPassport }}
          //   style={{ marginBottom: 20 }}
          name="password"
          value={form.password}
          onChange={handleChange}
        />

        <Link
          to={REGISTER_PATH}
          style={{ textAlign: "right", margin: "20px 0", cursor: "pointer", display: 'inline-block' }}
        >
          Inscription
        </Link>

        <Button
          title={"Se connecter"}
          onClick={(e) => handleLogin(e)}
          bgColor={'rgba(10,20,30,.8)'}
          color={"#fff"}
          loading={null}
        />
      </form>
    </div>
  );
})

export default LoginPage;

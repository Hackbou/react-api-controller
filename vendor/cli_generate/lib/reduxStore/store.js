import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';


const persistConfig = {
    key: 'root',
    storage,
}

const rootReducer = combineReducers({
    /*
    |--------------------------------------------------------------------------
    | Put here your reducers
    |--------------------------------------------------------------------------
    */
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
    reducer: persistedReducer,
    devTools: process.env !== 'production',
})

export const persistor = persistStore(store)

/*
|--------------------------------------------------------------------------
! All Your Application Navigations Paths Will Be Here
|--------------------------------------------------------------------------
*/
export const LOGIN_PATH = "/login";
export const REGISTER_PATH = "/register";
export const FORGETPASSWORD = "/auth/forgetPassword";
export const RESETPASSWORD = "/aut/resetPassword";

export const NOTFOUND_PATH = "*";

export const ACCEUIL_PATH = "/";
export const PRODUCTS_PATH = "/products";

import { ACCEUIL_PATH, PRODUCTS_PATH } from "./navigationPaths";
import { HiHome } from "react-icons/hi";

/*
|--------------------------------------------------------------------------
! Sidebar Items
|--------------------------------------------------------------------------
|
| Here we can add sidebar links, paths and icons
|
*/
export const links = [
    { path: ACCEUIL_PATH, icon: HiHome, label: "Acceuil" },
    { path: PRODUCTS_PATH, icon: HiHome, label: "Produits" },
]

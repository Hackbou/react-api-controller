import HomePage from "pages/home/HomePage";
import { ACCEUIL_PATH } from "../navigationPaths";

export const privateRoutes = [
    {
        path: ACCEUIL_PATH,
        element: <HomePage />,
    },
];
import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import RequireAuth from "../layouts/RequireAuth";
import { NOTFOUND_PATH } from "./navigation/navigationPaths";
import { publicRoutes } from "./navigation/navigationRoutes/public";
import { privateRoutes } from "./navigation/navigationRoutes/private";
import PageNotFound from "pages/notFound/PageNotFound";

/**
 * 
 * @param {*} routes 
 * with this recursive routing, you can nest any deep level routing as you want
 * @returns 
 */

const recursiveRoutes = (routes) => {
  return <>
    {
      routes.map((route, index) => {
        if (route.children) {
          return <Route path={route.path} element={route.element} key={index}>
            {recursiveRoutes(route.children)}
          </Route>
        }
        return <Route path={route.path} element={route.element} key={index} />
      })
    }
  </>
}

function Roots() {
  return (
    <BrowserRouter>
      <Routes>
        {
          recursiveRoutes(publicRoutes)
        }
        <Route element={<RequireAuth role={"user"} />}>
          {
            recursiveRoutes(privateRoutes)
          }
        </Route>
        <Route path={NOTFOUND_PATH} element={<PageNotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default Roots;

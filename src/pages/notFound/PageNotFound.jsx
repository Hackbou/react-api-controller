import React from "react";
import styled from "styled-components";

const NotFoundPane = styled.h1`
  text-align: center;
`;

const PageNotFound = () => {
  return <NotFoundPane>404</NotFoundPane>;
};

export default PageNotFound;

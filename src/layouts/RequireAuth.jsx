import React from "react";
import { Navigate, Outlet, useLocation } from "react-router-dom";
import getCurrentUser from "../hooks/getCurrentUser";
import NavbarComponent from "./navbar/NavbarComponent";
import SidebarComponent from "./sidebar/SidebarComponent";
import SpliTemplateScreen from ".";
import { NOTFOUND_PATH } from "../routes/navigation/navigationPaths";

function RequireAuth({ role }) {
  const user = getCurrentUser();

  const location = useLocation();

  return user?.role === role ? (
    <SpliTemplateScreen>
      <NavbarComponent />
      <SidebarComponent
        bgColor={"#fff"}
        activeLinkColor={"#fff"}
        activeLinkBgColor="rgba(0,5,10,.5)"
        linkColor="black"
        colorOnHover={"black"}
      />
      <Outlet />
    </SpliTemplateScreen>
  ) : (
    <Navigate to={NOTFOUND_PATH} state={{ from: location }} replace />
  );
}

export default RequireAuth;

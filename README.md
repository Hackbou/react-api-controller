<div align="center">
![RED Team](https://media.discordapp.net/attachments/1003601969463705691/1061686166941872128/dessin.png?width=591&height=595)
</div>

# Cli file Generator

The `file genrerator` will help you to generate all file you need if they exist on the programme.

## Usage

### Install dependencies
```bash
  npm run vendor
```

### Generate redux resource
```bash
  npm run make reduxResource <resourceName>
```

### Generate redux store
```bash
  npm run make redux <storeName>
```

## Required argument

`plop`

`file_type`

`file_name`

## FILE Reference

#### table of files

| Name    | Description                                 |
| :------ | :------------------------------------------ |
| `redux` | Generate redux files                        |
| `auth`  | Generate Auth files like login , signUp etc |

#### extra information

If you want to see extra files type :
Go to see redteam.config.json in vendor/cli_generate/redteam.config.json

## Deployment

To deploy this project run

```bash
  npm run deploy
```

## 🔗 Links

[![Depo reference](https://img.shields.io/badge/Github_reference-000?style=for-the-badge&logo=github&logoColor=white)](https://katherineoelsner.com/)
